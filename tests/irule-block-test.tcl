package require -exact testcl 1.0.14
namespace import ::testcl::*

before {
    class configure allowed_workstations_datagroup {
    "10.0.0.2" "nono"
  }
}

it "should handle access to /superola* (blocked)" {
  event HTTP_REQUEST
  on IP::client_addr return "10.0.0.1"
  HTTP::uri "/superola/hemligt"
  verify "Denied HTTP::status 403" "403" == {HTTP::status}
  run irule-block.irule block_irule
}

it "should handle access to /superola* (allowed)" {
  event HTTP_REQUEST
  on IP::client_addr return "10.0.0.2"
  HTTP::uri "/superola/hemligt"
  endstate pool allowed
  run irule-block.irule block_irule
}

it "should handle access to /nynet*" {
  event HTTP_REQUEST
  on IP::client_addr return "10.0.0.2"
  HTTP::uri "/nynet/test"
  endstate pool nynet
  run irule-block.irule block_irule
}

it "should handle access to /test2* (default case)" {
  event HTTP_REQUEST
  on IP::client_addr return "10.0.0.2"
  HTTP::uri "/test2/test"
  on snat automap return ""
  endstate pool vanligapoolen
  run irule-block.irule block_irule
}
