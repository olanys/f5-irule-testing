package require -exact testcl 1.0.14
namespace import ::testcl::*

it "should handle access to /superola* (allowed)" {
  event HTTP_REQUEST
  HTTP::uri "/uppstart/test"
  verify "no HTTP::status" "" == {HTTP::status}
  endstate pass
  run irule-uri.irule irule-uri
}

it "should handle access to /other* (redir to https)" {
  event HTTP_REQUEST
  HTTP::uri "/other/test"
  HTTP::header insert Host "test.site"
  verify "Redirect  HTTP::status 301" "301" == {HTTP::status}
  verify "Location: https://" "https://test.site/other/test" == {HTTP::header Location}
  run irule-uri.irule irule-uri
}
