[![pipeline status](https://gitlab.com/olanys/f5-irule-testing/badges/master/pipeline.svg)](https://gitlab.com/olanys/f5-irule-testing/-/commits/master)

# f5-irule-testing


Example of F% LTM iRule with irule-testing with TestTCL via docker-image

`docker run --read-only --rm -w /drone/src/tests -t -v $(pwd):/drone/src quay.io/olanystrom/testcl:1.0.14-2 jtcl irule-uri-test.tcl`

`docker run --read-only --rm -w /drone/src/tests -t -v $(pwd):/drone/src quay.io/olanystrom/testcl:1.0.14-2 jtcl irule-block-test.tcl`
